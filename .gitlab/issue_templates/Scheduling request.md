### Interviewer sets up their Calendly
* [ ] Come up with several time periods over the next two weeks that you'll be able to conduct interviews
* [ ] Go into Calendly and limit the bookable hours to those time periods 
  `Event types` -> select 30 min meeting -> `When can people book this event?` -> add your available times

![Screen_Shot_2019-10-16_at_9.25.23_AM](/uploads/fb250eb77cb32bc9e229f7439be8d49b/Screen_Shot_2019-10-16_at_9.25.23_AM.png)
* [ ] Share the link here for your 30 minute meeting

### @evhoffmann 
* [ ] Send out researcher's 30 minute meeting link 
* Invite x users with the following criteria:
  * Job title, org size, tools etc.
  * [ ] User 1
  * [ ] User 2
  * [ ] User 3
  * [ ] User 4