<!--Please answer the below questions to the best of your ability.-->

#### What's this issue all about?

#### What are the overarching goals for the research?

#### What hypotheses and/or assumptions do you have?

#### What questions are you trying to answer?

##### Core questions 

<!-- What needs to be answered to move work forward? -->

##### Additional questions

<!-- Is there anything else you'd like to know? -->

#### What persona, persona segment, or customer type experiences the problem most acutely?

#### What business decisions will be made based on this information?

#### What timescales do you have in mind for the research?

<!--Let us know the priority or urgency of the request so that we can plan and help you accordingly! -->

#### Who will be leading the research?

#### What is the link to your opportunity canvas? (Optional)
<!--If you have completed an opportunity canvas for this research study, please post the link to the file here -->

/label ~"Backlog", ~"workflow::problem validation"
